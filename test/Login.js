process.env.NODE_ENV="test";

let mongoose = require("mongoose");
let http=require('http');
let userModel = require("../models/user");

let chai=require("chai");
let chaiHttp = require("chai-http");
let server = require("../app");
let should = chai.should();

chai.use(chaiHttp);

describe("Login",() => {

  before((done) => {
    userModel.remove({},(error)=>{
      if(error)
        console.log("Error while clearing data:"+error);
      done();
    });
  });

  describe("/login",()=>{
    it("Should not login",(done)=>{
      let userBody={
        email:"hullesanket@gmail.com",
        password:"123456"
      }
      chai.request(http.createServer(server))
      .post("/api/user/login")
      .send(userBody)
      .end((error,response) =>{
        response.should.have.status(401);
        response.body.should.be.a('object');
        response.body.should.have.property('message');
        response.body.should.have.property('message').eql("Auth failed");
        done();
      });
    });

    it("Should create new user",(done)=>{
      let userBody={
        email:"hullesanket@gmail.com",
        password:"123456",
        isAdmin:true,
      }
      chai.request(http.createServer(server))
      .post("/api/user/signup")
      .send(userBody)
      .end((error,response) =>{
        response.should.have.status(201);
        response.body.should.be.a('object');
        response.body.should.have.property('result');
        done();
      });
    });
    it("Should able to login",(done)=>{
      let userBody={
        email:"hullesanket@gmail.com",
        password:"123456"
      }
      chai.request(http.createServer(server))
      .post("/api/user/login")
      .send(userBody)
      .end((error,response) =>{
        response.should.have.status(200);
        response.body.should.be.a('object');
        response.body.should.have.property('token');
        response.body.should.have.property('expiresIn').eql(3600);
        response.body.should.have.property('isAdmin').eql(true);
        done();
      });
    });
  });
});
