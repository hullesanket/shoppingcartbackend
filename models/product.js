const mongoose = require("mongoose");

const productSchema = mongoose.Schema({
  productName: { type: String, required: true },
  imagePath: { type: String, required: true },
  manufacturer: { type: String, required: true },
  price: { type: Number, required: true },
  quantity: { type: Number, required: true },
  productDescription:{ type: String, required: true },
  creator:{type:mongoose.Schema.Types.ObjectId,ref:"User",required:true}
});

module.exports = mongoose.model("Product", productSchema);
