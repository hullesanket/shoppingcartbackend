const express = require("express");

const checkAuth = require("../middleware/check-auth");
var productController = require("../controllers/product");

const router = express.Router();
const getImageFileMiddleWare= require("../middleware/getImageFile");

router.post(
  "/addProduct",
  checkAuth,
  getImageFileMiddleWare,
  productController.createProduct
);

router.patch("/updateProductStock",
  checkAuth,
  productController.UpdateProduct
);

router.get("", productController.getAllProduct);

router.get("/:id",productController.getProduct);

router.delete("/:id", checkAuth, productController.deleteProduct);

module.exports = router;
