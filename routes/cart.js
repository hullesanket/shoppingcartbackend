const express = require("express");

const checkAuth = require("../middleware/check-auth");
var cartController = require("../controllers/cart");

const router = express.Router();

router.post(
  "/addProductToCart",
  checkAuth,
  cartController.addToCart
);

router.get("/:userId",
  cartController.getAllAddedProducts
);
router.get("/getCartCount/:userId",
  cartController.getAddedCartCount
);
router.patch("/updateCart",
  checkAuth,
  cartController.updateCart
);
router.get("/removeProduct/:productId",
checkAuth,
  cartController.removeProductFromCart
);

module.exports = router;
