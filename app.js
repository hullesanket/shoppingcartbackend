const path = require("path");
const express = require("express");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");

const productsRoutes = require("./routes/product");
const userRoutes = require("./routes/user");
const cartRoutes = require("./routes/cart");
const config=require('config');

const app = express();

const dbUrl=`mongodb://${config.get('dbConfig.host')}/${config.get('dbConfig.dbName')}`;

mongoose.connect(dbUrl)
  .then(() => {
    console.log("Connected to database!: "+dbUrl );
  })
  .catch(() => {
    console.log("Connection failed!");
  });

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use("/images", express.static(path.join(__dirname,"images")));
app.use("/", express.static(path.join(__dirname,"dist")));

app.use((req, res, next) => {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept, Authorization"
  );
  res.setHeader(
    "Access-Control-Allow-Methods",
    "GET, POST, PATCH, PUT, DELETE, OPTIONS"
  );
  next();
});

app.use("/api/product", productsRoutes);
app.use("/api/user", userRoutes);
app.use("/api/cart", cartRoutes);
app.use(function(req,res,next){
  res.sendFile(path.join(__dirname,"dist","index.html"));
  //res.sendFile(path.join(__dirname,"dist","index.html"));
});

module.exports = app;
