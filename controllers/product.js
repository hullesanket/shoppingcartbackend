const Product = require("../models/product");
const cardModel= require("../models/cart");
let ProductController = {};

ProductController.createProduct = (req, res, next) => {
  const url = req.protocol + "://" + req.get("host");
  const product = new Product({
    productName: req.body.productName,
    manufacturer: req.body.manufacturer,
    price: req.body.price,
    quantity: req.body.quantity,
    productDescription: req.body.productDescription,
    imagePath: url + "/images/" + req.file.filename,
    creator:req.userData.userId
  });
  product.save().then(createdProduct => {
    res.status(201).json({
      message: "Product added successfully",
      Product: {
        ...createdProduct,
        id: createdProduct._id
      }
    });
  }).catch(err => {
    return res.status(401).json({
      message: "Auth failed"
    });
  });;
};
ProductController.UpdateProduct = (req, res, next) => {
  var updateQueryArray= [];
  console.log(req.body.productObjectArray);
  for(let i=0;i<req.body.productObjectArray.length;i++){
    let productObject = req.body.productObjectArray[i];
    updateQueryArray.push(Product.updateOne({ _id: productObject.productId },{"$inc":{'quantity':-productObject.quantity}} ));
  }

  Promise.all(updateQueryArray).then(function(values) {
    return cardModel.deleteMany({'creator':req.userData.userId});
  }).then(response =>{
    res.status(200).json({ message: "Order placed sucessfully"});
  }).catch(error => {
    res.status(500).json({ message: error.message});
  });
};
ProductController.deleteProduct =(req, res, next) => {
  Product.deleteOne({ _id: req.params.id,creator:req.userData.userId }).then(result => {
    if(result.n>0)
      res.status(200).json({ message: "Product deleted!" });
    else
      res.status(401).json({ message: "Unauthorized access" });
  });
} ;
ProductController.getProduct = (req, res, next) => {
  Product.findById(req.params.id).then(Product => {
    if (Product) {
      res.status(200).json(Product);
    } else {
      res.status(404).json({ message: "Product not found!" });
    }
  });
} ;
ProductController.getAllProduct = (req, res, next) => {
  const pageSize = +req.query.pagesize;
  const currentPage = +req.query.page;
  const sortBy = req.query.sortBy;
  const sortOrder = +req.query.sortOrder;
  let ProductQuery = Product.find();
  let fetchedProducts;

  console.log(sortBy);
  console.log(sortOrder);
  if(sortBy && sortOrder){
    var sortQueryObject={};
    sortQueryObject[sortBy] = sortOrder;
    ProductQuery.sort(sortQueryObject);
  }

  if (pageSize && currentPage) {
    ProductQuery.skip(pageSize * (currentPage - 1)).limit(pageSize);
  }
  ProductQuery
    .then(documents => {
      fetchedProducts = documents;
      return Product.count();
    })
    .then(count => {
      res.status(200).json({
        message: "Products fetched successfully!",
        products: fetchedProducts,
        maxProducts: count
      });
    }).catch(err => {
      return res.status(500).json({
        message: "Something got wrog while getting Product details"
      });
    });;
};

module.exports = ProductController;
