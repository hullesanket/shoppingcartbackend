const Cart = require("../models/cart");
const _ = require('underscore');
let cartController = {};

cartController.addToCart = (req, res, next) => {
  const url = req.protocol + "://" + req.get("host");
  const cart = new Cart({
    productId: req.body.productId,
    quantity: 1,
    creator:req.userData.userId
  });
  cart.save().then(createdCart => {
    res.status(201).json({
      message: "Cart added successfully",
      cart: {
        ...createdCart,
        id: createdCart._id
      }
    });
  }).catch(err => {
    return res.status(401).json({
      message: "Auth failed"
    });
  });
};

cartController.updateCart = (req, res, next) => {
  let filterQuery = {creator:req.userData.userId,productId: req.body.productId};
  let updateData = {quantity:req.body.quantity};
  const cart = Cart.updateOne(filterQuery,{$set:updateData}).then(result => {
  if(result.n>0)
      res.status(200).json({ message: "Update successful!" });
    else
      res.status(401).json({ message: "Unauthorized access" });
  }).catch(err => {
    return res.status(401).json({
      message: "Auth failed"
    });
  });;
};

cartController.getAllAddedProducts = (req, res, next) => {
  let cartQuery = Cart.find({creator:req.params.userId});

  cartQuery.
  populate('productId')
  .then(carts => {
    if(carts.length==0){
      return res.status(200).json({
        message: "carts fetched successfully!",
        'orderdetails': []
      })
    }
    var cartArray =carts.map(mappedObject=>{
      return{
        _id:mappedObject.id,
        productId:mappedObject.productId._id,
        productName:mappedObject.productId.productName,
        productDescription:mappedObject.productId.productDescription,
        price:mappedObject.productId.price,
        quantity:mappedObject.quantity,
        imagePath:mappedObject.productId.imagePath,
        stockQuantity:mappedObject.productId.quantity
      }
    });
    res.status(200).json({
        message: "carts fetched successfully!",
        'orderdetails': cartArray
      });
    }).catch(err => {
      return res.status(500).json({
        message: "Something got wrog while getting cart details"
      });
    });;
};

cartController.getAddedCartCount = (req, res, next) => {
  let cartQuery = Cart.find({creator:req.params.userId},{"_id":0,"productId":1,"quantity":1});

  cartQuery
  .then(cartDetails => {
      res.status(200).json({
        message: "carts fetched successfully!",
        'cartCount': cartDetails.length,
        'productId':_.flatten(_.pluck(cartDetails,"productId"))
      });
    }).catch(err => {
      return res.status(500).json({
        message: err.message
      });
    });;
};

cartController.removeProductFromCart =(req, res, next) => {
  Cart.deleteOne({ productId: req.params.productId,creator:req.userData.userId }).then(result => {
    console.log(result);
    if(result.n>0)
      res.status(200).json({ message: "Product deleted!" });
    else
      res.status(401).json({ message: "Unauthorized access" });
  });
} ;

module.exports = cartController;
